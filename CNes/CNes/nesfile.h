#pragma once
#include <stdio.h>

typedef struct _nes_file_header
{
	unsigned char identifier[4];
	unsigned _int8 pgrNumber : 8;
	unsigned _int8 chrNumber : 8;
	unsigned _int8 romControlByte1 : 8;
	unsigned _int8 romControlByte2 : 8;
	unsigned _int8 ramBanksNumber : 8;
	unsigned _int8 nesControlByte : 8;
	unsigned char reserved[6];
} NesFileHeader;

typedef NesFileHeader* NesFileHeaderPtr;

typedef struct _nes_file 
{
	NesFileHeaderPtr header;
	unsigned __int8** pgrBank;
	unsigned __int8** chrBank;
} NesFile;

typedef NesFile* NesFilePtr;

//private
void loadPgrBanks(FILE* file, NesFilePtr nesFile);
void loadChrBanks(FILE* file, NesFilePtr nesFile);

//public
NesFilePtr loadNesFile(char* path);
void freeNesFile(NesFilePtr nesFile);
