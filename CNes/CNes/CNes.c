#include "blogic.h"
#include "nesfile.h"
#include "opcodes.h"

void printBinaryString(__int8 val);
void printBinaryString(__int8 val)
{
	for (int i = 7; i >= 0; i--)
	{
		printf("%i", getBit8(val, i));
	}
}

int main()
{
	NesFilePtr nesFilePtr = loadNesFile("e:/smb.nes");
	int index = 0;
	unsigned _int8 bytes;
	while(index < (16 * 1024))
	{
		int opno = nesFilePtr->pgrBank[0][index++];
		OPCODE opcode = OPCODES[opno];

		bytes = ADDR_MODE_BTS[opcode.addrMode];
		bytes--;

		printf("opno %x opcode %s arguments %i [", opno, opcode.mnemonic, bytes);

		while (bytes > 0) {
			bytes--;
			printf(" arg:%x", (unsigned __int8)(nesFilePtr->pgrBank[0][index++]));
		}

		printf("]\n");
	}
	freeNesFile(nesFilePtr);

	getch();
}