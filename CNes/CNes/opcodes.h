#pragma once

#define KILL { "KIL", IMPL, 0, false }

#define A_BTS 1
#define IMM_BTS 2
#define IMPL_BTS 1
#define IND_BTS 2
#define IND_X_BTS 2
#define IND_Y_BTS 2
#define ZPG_BTS 2
#define ZPG_X_BTS 2
#define ZPG_Y_BTS 2
#define ABS_BTS 3
#define ABS_X_BTS 3
#define ABS_Y_BTS 3
#define REL_BTS 2

#define boolean unsigned __int8
#define true 1
#define false 0

enum AddrMode
{
	A, IMM, IMPL, IND, IND_X, IND_Y, ZPG, ZPG_X, ZPG_Y, ABS, ABS_X, ABS_Y, REL
};

typedef struct _opcode
{
	char* mnemonic;
	enum AddrMode addrMode;
	int cycles;
	boolean additionalCycles;
} OPCODE;

int ADDR_MODE_BTS[13];

OPCODE OPCODES[256];
