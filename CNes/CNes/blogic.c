#include "blogic.h"

int getBit16(__int16 val, int index)
{
	int mask = 1 << index;
	return (val & mask) == mask;
}

int getBit8(__int8 val, int index)
{
	int mask = 1 << index;
	return (val & mask) == mask;
}

void turnOnBit8(__int8* val, int index)
{
	int mask = 1 << index;
	*val |= mask;
}

void turnOffBit8(__int8* val, int index)
{
	int mask = 1 << index;
	*val &= ~mask;
}
