#pragma once

#define STATUS_REGISTER_N_BIT 7
#define STATUS_REGISTER_V_BIT 6
#define STATUS_REGISTER_B_BIT 4
#define STATUS_REGISTER_D_BIT 3
#define STATUS_REGISTER_I_BIT 2
#define STATUS_REGISTER_Z_BIT 1
#define STATUS_REGISTER_C_BIT 0

typedef struct _status_register
{
	unsigned __int8 N : 1;
	unsigned __int8 V : 1;
	unsigned __int8 _ : 1;
	unsigned __int8 B : 1;
	unsigned __int8 D : 1;
	unsigned __int8 I : 1;
	unsigned __int8 Z : 1;
	unsigned __int8 C : 1;
} StatusRegister;

typedef StatusRegister* StatusRegisterPtr;

typedef struct _registers
{
	unsigned __int8 A;
	unsigned __int8 Y;
	unsigned __int8 X;
	unsigned __int16 PC;
	unsigned __int8 S;
	StatusRegisterPtr P;
} Registers;

typedef Registers* RegistersPtr;

typedef struct _cpu
{
	RegistersPtr registers;
} CPU;

typedef CPU* CPUPtr;

//public
CPUPtr newCPU();
void freeCPU(CPUPtr cpu);