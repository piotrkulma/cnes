#pragma once
int getBit16(__int16 val, int index);

int getBit8(__int8 val, int index);
void turnOnBit8(__int8* val, int index);
void turnOffBit8(__int8* val, int index);
