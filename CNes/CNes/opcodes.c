#include "opcodes.h"

int ADDR_MODE_BTS[13] =
{
	A_BTS, IMM_BTS, IMPL_BTS, IND_BTS, IND_X_BTS, IND_Y_BTS, ZPG_BTS, ZPG_X_BTS, ZPG_Y_BTS, ABS_BTS, ABS_X_BTS, ABS_Y_BTS, REL_BTS
};

OPCODE OPCODES[256] =
{
	{ "BRK", IMPL, 7, false },//dec:0 hex:0
	{ "ORA", IND_X, 6, false },//dec:1 hex:1
	KILL,//dec:2 hex:2 unofficial
	{ "SLO", IND_X, 8, false },//dec:3 hex:3 unofficial
	{ "NOP", ZPG, 3, false },//dec:4 hex:4 unofficial
	{ "ORA", ZPG, 3, false },//dec:5 hex:5
	{ "ASL", ZPG, 5, false },//dec:6 hex:6
	{ "SLO", ZPG, 5, false},//dec:7 hex:7 unofficial
	{ "PHP", IMPL, 3, false },//dec:8 hex:8
	{ "ORA", IMM, 2, false },//dec:9 hex:9
	{ "ASL", A, 2, false },//dec:10 hex:a
	{ "ANC", IMM, 2, false},//dec:11 hex:b unofficial
	{ "NOP", ABS, 4, false},//dec:12 hex:c unofficial
	{ "ORA", ABS, 4, false },//dec:13 hex:d
	{ "ASL", ABS, 6, false },//dec:14 hex:e
	{ "SLO", ABS, 6, false },//dec:15 hex:f
	{ "BPL", REL, 2, true },//dec:16 hex:10
	{ "ORA", IND_Y, 5, true },//dec:17 hex:11
	KILL,//dec:18 hex:12 unofficial
	{ "SLO", IND_Y, 8, false },//dec:19 hex:13 unofficial
	{ "NOP", ZPG_X, 4, false },//dec:20 hex:14 unofficial
	{ "ORA", ZPG_X, 4, false },//dec:21 hex:15
	{ "ASL", ZPG_X, 6, false },//dec:22 hex:16
	{ "SLO", ZPG_X, 6, false },//dec:23 hex:17 unofficial
	{ "CLC", IMPL, 2, false },//dec:24 hex:18
	{ "ORA", ABS_Y, 4, true },//dec:25 hex:19
	{ "NOP", IMPL, 2, false },//dec:26 hex:1a unofficial
	{ "SLO", ABS_Y, 7, false },//dec:27 hex:1b unofficial
	{ "NOP", ABS_X, 4, true },//dec:28 hex:1c unofficial
	{ "ORA", ABS_X, 4, true },//dec:29 hex:1d
	{ "ASL", ABS_X, 7, false },//dec:30 hex:1e
	{ "SLO", ABS_X, 7, false },//dec:31 hex:1f unofficial
	{ "JSR", ABS, 6, false },//dec:32 hex:20
	{ "AND", IND_X, 6, false },//dec:33 hex:21
	KILL,//dec:34 hex:22 unofficial
	{ "RLA", IND_X, 8, false },//dec:35 hex:23 unofficial
	{ "BIT", ZPG, 3, false },//dec:36 hex:24
	{ "AND", ZPG, 3, false },//dec:37 hex:25
	{ "ROL", ZPG, 5, false },//dec:38 hex:26
	{ "RLA", ZPG, 5, false },//dec:39 hex:27 unofficial
	{ "PLP", IMPL, 4, false },//dec:40 hex:28
	{ "AND", IMM, 2, false },//dec:41 hex:29
	{ "ROL", A, 2, false },//dec:42 hex:2a
	{ "ANC", IMM, 2, false},//dec:43 hex:2b unofficial
	{ "BIT", ABS, 4, false },//dec:44 hex:2c
	{ "AND", ABS, 4, false },//dec:45 hex:2d
	{ "ROL", ABS, 6, false },//dec:46 hex:2e
	{ "RLA", ABS, 6, false },//dec:47 hex:2f unofficial
	{ "BMI", REL, 2, true },//dec:48 hex:30
	{ "AND", IND_Y, 5, true },//dec:49 hex:31
	KILL,//dec:50 hex:32 unofficial
	{ "RLA", IND_Y, 4, false },//dec:51 hex:33 unofficial
	{ "NOP", ZPG_X, 4, false },//dec:52 hex:34 unofficial
	{ "AND", ZPG_X, 4, false },//dec:53 hex:35
	{ "ROL", ZPG_X, 6, false },//dec:54 hex:36
	{ "RLA", ZPG_X, 6, false },//dec:55 hex:37 unofficial
	{ "SEC", IMPL, 2, false },//dec:56 hex:38
	{ "AND", ABS_Y, 4, true },//dec:57 hex:39
	{ "NOP", IMPL, 2, false },//dec:58 hex:3a unofficial
	{ "RLA", ABS_Y, 7, false},//dec:59 hex:3b unofficial
	{ "NOP", ABS_X, 4, true },//dec:60 hex:3c unofficial
	{ "AND", ABS_X, 4, true },//dec:61 hex:3d
	{ "ROL", ABS_X, 7, false },//dec:62 hex:3e
	{ "RLA", ABS_X, 7, false },//dec:63 hex:3f unofficial
	{ "RTI", IMPL, 6, false },//dec:64 hex:40
	{ "EOR", IND_X, 6, false },//dec:65 hex:41
	{ "KIL", IMM, 0, false },//dec:66 hex:42 unofficial
	{ "SRE", IND_X, 8, false },//dec:67 hex:43 unofficial
	{ "NOP", ZPG, 8, false },//dec:68 hex:44 unofficial
	{ "EOR", ZPG, 3, false },//dec:69 hex:45
	{ "LSR", ZPG, 5, false },//dec:70 hex:46
	{ "SRE", ZPG, 5, false },//dec:71 hex:47 unofficial
	{ "PHA", IMPL, 3, false },//dec:72 hex:48
	{ "EOR", IMM, 2, false },//dec:73 hex:49
	{ "LSR", A, 2, false },//dec:74 hex:4a
	{ "ALR", IMM, 2, false },//dec:75 hex:4b unofficial
	{ "JMP", ABS, 3, false },//dec:76 hex:4c
	{ "EOR", ABS, 4, false },//dec:77 hex:4d
	{ "LSR", ABS, 6, false },//dec:78 hex:4e
	{ "SRE", ABS, 6, false },//dec:79 hex:4f unofficial
	{ "BVC", REL, 2, true },//dec:80 hex:50
	{ "EOR", IND_Y, 5, true },//dec:81 hex:51
	KILL,//dec:82 hex:52 unofficial
	{ "SRE", IND_Y, 8, false },//dec:83 hex:53 unofficial
	{ "NOP", ZPG_X, 4, false },//dec:84 hex:54 unofficial
	{ "EOR", ZPG_X, 4, false },//dec:85 hex:55
	{ "LSR", ZPG_X, 6, false },//dec:86 hex:56
	{ "SRE", ZPG_X, 6, false },//dec:87 hex:57 unofficial
	{ "CLI", IMPL, 2, false },//dec:88 hex:58
	{ "EOR", ABS_Y, 4, true },//dec:89 hex:59
	{ "NOP", IMPL, 2, false },//dec:90 hex:5a unofficial
	{ "SRE", ABS_Y, 7, false },//dec:91 hex:5b unofficial
	{ "NOP", ABS_X, 4, true },//dec:92 hex:5c unofficial
	{ "EOR", ABS_X, 4, true },//dec:93 hex:5d
	{ "LSR", ABS_X, 7, false },//dec:94 hex:5e
	{ "SRE", ABS_X, 7, false },//dec:95 hex:5f unofficial
	{ "RTS", IMPL, 5, false },//dec:96 hex:60
	{ "ADC", IND_X, 6, false },//dec:97 hex:61
	KILL,//dec:98 hex:62 unofficial
	{ "RRA", IND_X, 8, false },//dec:99 hex:63 unofficial
	{ "NOP", ZPG, 3, false },//dec:100 hex:64 unofficial
	{ "ADC", ZPG, 3, false },//dec:101 hex:65
	{ "ROR", ZPG, 5, false },//dec:102 hex:66
	{ "RRA", ZPG, 5, false },//dec:103 hex:67 unofficial
	{ "PLA", IMPL, 4, false },//dec:104 hex:68
	{ "ADC", IMM, 2, false },//dec:105 hex:69
	{ "ROR", A, 2, false },//dec:106 hex:6a
	{ "ARR", IMM, 2, false },//dec:107 hex:6b unofficial
	{ "JMP", IND, 5, false },//dec:108 hex:6c
	{ "ADC", ABS, 4, false },//dec:109 hex:6d
	{ "ROR", ABS, 6, false },//dec:110 hex:6e
	{ "RRA", ABS, 6, false },//dec:111 hex:6f unofficial
	{ "BVS", REL, 2, true },//dec:112 hex:70
	{ "ADC", IND_Y, 5, true },//dec:113 hex:71
	KILL,//dec:114 hex:72 unofficial
	{ "RRA", IND_Y, 8, false },//dec:115 hex:73 unofficial
	{ "NOP", ZPG_X, 4, false },//dec:116 hex:74 unofficial
	{ "ADC", ZPG_X, 4, false },//dec:117 hex:75
	{ "ROR", ZPG_X, 6, false },//dec:118 hex:76
	{ "BRA", ZPG_X, 6, false },//dec:119 hex:77 unofficial
	{ "SEI", IMPL, 2, false },//dec:120 hex:78
	{ "ADC", ABS_Y, 4, true },//dec:121 hex:79
	{ "NOP", IMPL, 2, false },//dec:122 hex:7a unofficial
	{ "RRA", ABS_Y, 7, false },//dec:123 hex:7b unofficial
	{ "NOP", ABS_X, 4, true },//dec:124 hex:7c unofficial
	{ "ADC", ABS_X, 4, true },//dec:125 hex:7d
	{ "ROR", ABS_X, 7, false },//dec:126 hex:7e
	{ "RRA", ABS_X, 7, false },//dec:127 hex:7f unofficial
	{ "NOP", IMM, 2, false },//dec:128 hex:80 unofficial
	{ "STA", IND_X, 6, false },//dec:129 hex:81
	{ "NOP", IMM, 2, false },//dec:130 hex:82 unofficial
	{ "SAX", IND_X, 6, false },//dec:131 hex:83 unofficial
	{ "STY", ZPG, 3, false },//dec:132 hex:84
	{ "STA", ZPG, 3, false },//dec:133 hex:85
	{ "STX", ZPG, 3, false },//dec:134 hex:86
	{ "SAX", ZPG, 3, false },//dec:135 hex:87 unofficial
	{ "DEY", IMPL, 2, false },//dec:136 hex:88
	{ "NOP", IMM, 2, false },//dec:137 hex:89 unofficial
	{ "TXA", IMPL, 2, false },//dec:138 hex:8a
	{ "XAA", IMM, 2, false },//dec:139 hex:8b unofficial
	{ "STY", ABS, 4, false },//dec:140 hex:8c
	{ "STA", ABS, 4, false },//dec:141 hex:8d
	{ "STX", ABS, 4, false },//dec:142 hex:8e
	{ "SAX", ABS, 4, false },//dec:143 hex:8f unofficial
	{ "BCC", REL, 2, true },//dec:144 hex:90
	{ "STA", IND_Y, 6, false },//dec:145 hex:91
	KILL,//dec:146 hex:92 unofficial
	{ "AHX", IND_Y, 6, false },//dec:147 hex:93 unofficial
	{ "STY", ZPG_X, 4, false },//dec:148 hex:94
	{ "STA", ZPG_X, 4, false },//dec:149 hex:95
	{ "STX", ZPG_Y, 4, false },//dec:150 hex:96
	{ "SAX", ZPG_Y, 4, false },//dec:151 hex:97 unofficial
	{ "TYA", IMPL, 3, false },//dec:152 hex:98
	{ "STA", ABS_Y, 5, false },//dec:153 hex:99
	{ "TXS", IMPL, 2, false },//dec:154 hex:9a
	{ "TAS", ABS_Y, 5, false },//dec:155 hex:9b unofficial
	{ "SHY", ABS_X, 5, false },//dec:156 hex:9c unofficial
	{ "STA", ABS_X, 5, false },//dec:157 hex:9d
	{ "SHX", ABS_Y, 5, false },//dec:158 hex:9e unofficial
	{ "AHX", ABS_Y, 5, false },//dec:159 hex:9f unofficial
	{ "LDY", IMM, 2, false },//dec:160 hex:a0
	{ "LDA", IND_X, 6, false },//dec:161 hex:a1
	{ "LDX", IMM, 2, false },//dec:162 hex:a2
	{ "LAX", IND_X, 6, false },//dec:163 hex:a3 unofficial
	{ "LDY", ZPG, 3, false },//dec:164 hex:a4
	{ "LDA", ZPG, 3, false },//dec:165 hex:a5
	{ "LDX", ZPG, 3, false },//dec:166 hex:a6
	{ "LAX", ZPG, 3, false },//dec:167 hex:a7 unofficial
	{ "TAY", IMPL, 2, false },//dec:168 hex:a8
	{ "LDA", IMM, 2, false },//dec:169 hex:a9
	{ "TAX", IMPL, 2, false },//dec:170 hex:aa
	{ "LAX", IMM, 2, false },//dec:171 hex:ab unofficial
	{ "LDY", ABS, 4, false },//dec:172 hex:ac
	{ "LDA", ABS, 4, false },//dec:173 hex:ad
	{ "LDX", ABS, 4, false },//dec:174 hex:ae
	{ "LAX", ABS, 4, false },//dec:175 hex:af unofficial
	{ "BCS", REL, 2, true },//dec:176 hex:b0
	{ "LDA", IND_Y, 5, true },//dec:177 hex:b1
	KILL,//dec:178 hex:b2 unofficial
	{ "LAX", IND_Y, 5, false },//dec:179 hex:b3 unofficial
	{ "LDY", ZPG_X, 4, false },//dec:180 hex:b4
	{ "LDA", ZPG_X, 4, false },//dec:181 hex:b5
	{ "LDX", ZPG_Y, 4, false },//dec:182 hex:b6
	{ "LAX", ZPG_Y, 4, false },//dec:183 hex:b7 unofficial
	{ "CLV", IMPL, 2, false },//dec:184 hex:b8
	{ "LDA", ABS_Y, 4, true },//dec:185 hex:b9
	{ "TSX", IMPL, 2, false },//dec:186 hex:ba
	{ "LAS", ABS_Y, 4, true},//dec:187 hex:bb unofficial
	{ "LDY", ABS_X, 4, true },//dec:188 hex:bc
	{ "LDA", ABS_X, 4, true },//dec:189 hex:bd
	{ "LDX", ABS_Y, 4, true },//dec:190 hex:be
	{ "LAX", ABS_Y, 4, true },//dec:191 hex:bf unofficial
	{ "CPY", IMM, 2, false },//dec:192 hex:c0
	{ "CMP", IND_X, 6, false },//dec:193 hex:c1
	{ "NOP", IMM, 2, false },//dec:194 hex:c2 unofficial
	{ "DCP", IND_X, 8, false },//dec:195 hex:c3 unofficial
	{ "CPY", ZPG, 3, false },//dec:196 hex:c4
	{ "CMP", ZPG, 3, false },//dec:197 hex:c5
	{ "DEC", ZPG, 5, false },//dec:198 hex:c6
	{ "DCP", ZPG, 5, false },//dec:199 hex:c7 unofficial
	{ "INY", IMPL, 2, false },//dec:200 hex:c8
	{ "CMP", IMM, 2, false },//dec:201 hex:c9
	{ "DEX", IMPL, 2, false },//dec:202 hex:ca
	{ "AXS", IMM, 2, false },//dec:203 hex:cb unofficial
	{ "CPY", ABS, 4, false },//dec:204 hex:cc
	{ "CMP", ABS, 4, false },//dec:205 hex:cd
	{ "DEC", ABS, 4, false },//dec:206 hex:ce
	{ "DCP", ABS, 6, false },//dec:207 hex:cf unofficial
	{ "BNE", REL, 2, true },//dec:208 hex:d0
	{ "CMP", IND_Y, 5, true },//dec:209 hex:d1
	KILL,//dec:210 hex:d2 unofficial
	{ "DCP", IND_Y, 4, false },//dec:211 hex:d3 unofficial
	{ "NOP", ZPG_X, 4, false },//dec:212 hex:d4 unofficial
	{ "CMP", ZPG_X, 4, false },//dec:213 hex:d5
	{ "DEC", ZPG_X, 6, false },//dec:214 hex:d6
	{ "DCP", ZPG_X, 5, false },//dec:215 hex:d7 unofficial
	{ "CLD", IMPL, 2, false },//dec:216 hex:d8
	{ "CMP", ABS_Y, 4, true },//dec:217 hex:d9
	{ "NOP", IMPL, 2, false },//dec:218 hex:da unofficial
	{ "DCP",ABS_Y, 7, false },//dec:219 hex:db unofficial
	{ "NOP", ABS_X, 4, true },//dec:220 hex:dc unofficial
	{ "CMP", ABS_X, 4, true },//dec:221 hex:dd
	{ "DEC", ABS_X, 7, false },//dec:222 hex:de
	{ "DCP", ABS_X, 7, false },//dec:223 hex:df unofficial
	{ "CPX", IMM, 2, false },//dec:224 hex:e0
	{ "SBC", IND_X, 6, false },//dec:225 hex:e1
	{ "NOP", IMM, 2, false },//dec:226 hex:e2 unofficial
	{ "ISC", IND_X, 8, false },//dec:227 hex:e3 unofficial
	{ "CPX", ZPG, 3, false },//dec:228 hex:e4
	{ "SBC", ZPG, 3, false },//dec:229 hex:e5
	{ "INC", ZPG, 5, false },//dec:230 hex:e6
	{ "ISC", ZPG, 5, false },//dec:231 hex:e7 unofficial
	{ "INX", IMPL, 2, false },//dec:232 hex:e8
	{ "SBC", IMM, 2, false },//dec:233 hex:e9
	{ "NOP", IMPL, 2, false },//dec:234 hex:ea
	{ "SBC", IMM, 2, false },//dec:235 hex:eb unofficial
	{ "CPX", ABS, 4, false },//dec:236 hex:ec
	{ "SBC", ABS, 4, false },//dec:237 hex:ed
	{ "INC", ABS, 6, false },//dec:238 hex:ee
	{ "ISC", ABS, 6, false },//dec:239 hex:ef unofficial
	{ "BEQ", REL, 2, true },//dec:240 hex:f0
	{ "SBC", IND_Y, 5, true },//dec:241 hex:f1
	KILL,//dec:242 hex:f2 unofficial
	{ "ISC", IND_Y, 8, false },//dec:243 hex:f3 unofficial
	{ "NOP", ZPG_X, 4, false },//dec:244 hex:f4 unofficial
	{ "SBC", ZPG_X, 4, false },//dec:245 hex:f5
	{ "INC", ZPG_X, 6, false },//dec:246 hex:f6
	{"ISC", ZPG_X, 6, false },//dec:247 hex:f7 unofficial
	{ "SED", IMPL, 2, false },//dec:248 hex:f8
	{ "SBC", ABS_Y, 4, true },//dec:249 hex:f9
	{ "NOP", IMPL, 2, false },//dec:250 hex:fa unofficial
	{ "ISC", ABS_Y, 7, false },//dec:251 hex:fb unofficial
	{ "NOP", ABS_X, 4, true },//dec:252 hex:fc unofficial
	{ "SBC", ABS_X, 4, true },//dec:253 hex:fd
	{ "INC", ABS_X, 7, false },//dec:254 hex:fe
	{ "ISC", ABS_X, 7, false }//dec:255 hex:ff unofficial
};