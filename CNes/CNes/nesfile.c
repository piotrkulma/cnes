#include "nesfile.h"

void loadPgrBanks(FILE* file, NesFilePtr nesFile)
{
	nesFile->pgrBank = malloc(sizeof(unsigned __int8*) * nesFile->header->pgrNumber);

	for (int i = 0; i < nesFile->header->pgrNumber; i++)
	{
		nesFile->pgrBank[i] = malloc(sizeof(unsigned __int8) * 16 * 1024);
		fread(nesFile->pgrBank[i], sizeof(unsigned __int8), 16 * 1024, file);
	}
}

void loadChrBanks(FILE* file, NesFilePtr nesFile)
{
	nesFile->chrBank = malloc(sizeof(unsigned __int8*) * nesFile->header->chrNumber);

	for (int i = 0; i < nesFile->header->chrNumber; i++)
	{
		nesFile->chrBank[i] = malloc(sizeof(unsigned __int8) * 8 * 1024);
		fread(nesFile->chrBank[i], sizeof(unsigned __int8), 8 * 1024, file);
	}
}

NesFilePtr loadNesFile(char* path)
{
	FILE* file = fopen(path, "rb");

	NesFilePtr nesFile = malloc(sizeof(NesFile));
	nesFile->header = malloc(sizeof(NesFileHeader));

	fread(nesFile->header, sizeof(NesFileHeader), 1, file);

	loadPgrBanks(file, nesFile);
	loadChrBanks(file, nesFile);

	__int8 i;
	while (!feof(file))
	{
		fread(&i, sizeof(_int8), 1, file);
		printf("%i", i);
	}

	fclose(file);

	return nesFile;
}

void freeNesFile(NesFilePtr nesFile)
{

	if (nesFile->chrBank != NULL)
	{
		for (int i = 0; i < nesFile->header->chrNumber; i++)
		{
			free(nesFile->chrBank[i]);
		}

		free(nesFile->chrBank);
	}

	if (nesFile->pgrBank != NULL)
	{
		for (int i = 0; i < nesFile->header->pgrNumber; i++)
		{
			free(nesFile->pgrBank[i]);
		}

		free(nesFile->pgrBank);
	}

	free(nesFile->header);
	free(nesFile);
}