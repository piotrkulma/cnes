#include "cpu.h"
#include <stdio.h>

CPUPtr newCPU()
{
	CPUPtr cpu = malloc(sizeof(CPU) * 1);
	cpu->registers = malloc(sizeof(CPUPtr) * 1);
	cpu->registers->P = malloc(sizeof(StatusRegister) * 1);
	return cpu;
}

void freeCPU(CPUPtr cpu)
{
	free(cpu->registers->P);
	free(cpu->registers);
	free(cpu);
}
